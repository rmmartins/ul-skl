import streamlit as st
import sklearn.cluster, sklearn.manifold
from sklearn import datasets, preprocessing
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import altair as alt
import pandas as pd
import numpy as np

DATASETS = ["iris", "boston", "diabetes", "digits", "wine", "breast_cancer"]
DR_METHODS = ["PCA", "TSNE", "Isomap", "LocallyLinearEmbedding", "MDS", "SpectralEmbedding"]
CLUSTERING = ['KMeans', 'AffinityPropagation', 'MeanShift', 'SpectralClustering', 'AgglomerativeClustering', 
    'DBSCAN', 'OPTICS']

# Streamlit encourages well-structured code, like starting execution in a main() function.
def main():
    # Setup the pipeline
    # load_dataset_cached = st.cache(load_dataset)    
    project_cached = st.cache(project)
    cluster_cached = st.cache(cluster)

    # UI    
    st.sidebar.title("Unsupervised Learning with Scikit-Learn")    
    dataset_name = st.sidebar.selectbox("Dataset", DATASETS)
    normalize = st.sidebar.checkbox("Normalize Features", True)    
    df, target = load_dataset(dataset_name, normalize)
    dr_name = st.sidebar.selectbox("Dim. Reduction", DR_METHODS)    
    cl_name = st.sidebar.selectbox("Clustering", CLUSTERING)
    color_opt = st.sidebar.selectbox("Color", ['clusters', 'target'], index=0)

    # Run    
    P = project_cached(df, dr_name)
    cl_labels = cluster_cached(df, cl_name)

    # View
    fig, ax = plt.subplots()   
    
    if color_opt == 'clusters':
        color_labels = list(range(len(cl_labels)))
        colors = cl_labels 
    else:
        color_labels = list(set(target))
        colors = [color_labels.index(x) for x in target]
    qual_cmap = plt.cm.get_cmap('tab10')
    
    color_count = np.unique(colors).shape[0]
    if color_count <= len(qual_cmap.colors):        
        cmap = ListedColormap(qual_cmap.colors[:color_count])
    else:
        cmap = plt.cm.get_cmap('viridis')   
    
    scatter = ax.scatter(*P.T, c=colors, cmap=cmap)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])    
    
    artists = scatter.legend_elements()[0]
    legend1 = ax.legend(artists, color_labels,
                    title=color_opt, loc='center left', bbox_to_anchor=(1, 0.5))
    ax.add_artist(legend1)

    plt.axis('off')
    st.pyplot()
    
    st.write(df)

def load_dataset(dataset_name, normalize=False):
    dataset = getattr(datasets, f"load_{dataset_name}")()
    data = dataset.data
    if normalize:
        data = preprocessing.MinMaxScaler().fit_transform(data)
    columns = dataset.feature_names if hasattr(dataset, 'feature_names') else None    
    df = pd.DataFrame(data, columns=columns)
    target = dataset.target
    if hasattr(dataset, 'target_names'):
        target = [dataset.target_names[x] for x in target]
    return df, target

def project(df, dr_name):
    if dr_name == "PCA":
        dr = PCA(n_components=2)
    else:
        dr = getattr(sklearn.manifold, dr_name)(n_components=2)        
    return dr.fit_transform(df)

def cluster(df, cl_name):
    cl = getattr(sklearn.cluster, cl_name)()
    return cl.fit_predict(df)

    
if __name__ == "__main__":
    main()
