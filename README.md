# ul-skl

Quick preview of unsupervised learning methods available in scikit-learn. Implemented as an exercise to learn streamlit.

## Install

`pip -r requirements.txt`

## Run

`streamlit run main.py`